import 'package:flutter/material.dart';
import 'package:pdex_app/Screens/pickup_information.dart';
import 'package:pdex_app/Screens/shop_information.dart';

class OpeningReason extends StatefulWidget {
  @override
  _OpeningReasonState createState() => _OpeningReasonState();
}

class _OpeningReasonState extends State<OpeningReason> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: GestureDetector(
          onTap: () {
            Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => ShopInformation()));
          },
          child: Icon(
            Icons.arrow_back,
            size: 30,
            color: Colors.white,
          ),
        ),
        title: Text('Opening Reason'),

      ),
      body: ListView(
        children: [
          ButtonTheme(
      minWidth: 200.0,
          height: 70.0,
          child:RaisedButton(
          elevation: 5,
          child: Text('BUSINESS USE', style: TextStyle(fontSize: 20),),
          color: Colors.white,
          textColor: Colors.black,
          onPressed: (){
            Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => PickUpInformation()));
          }),
    ),

    ButtonTheme(
      minWidth: 200.0,
          height: 70.0,
          child:RaisedButton(
          elevation: 5,
          child: Text('PERSONAL USE', style: TextStyle(fontSize: 20),),
          color: Colors.white,
          textColor: Colors.black,
          onPressed: (){
            Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => PickUpInformation()));
          }),
    ),
        ],
      ),
    );
  }
}
