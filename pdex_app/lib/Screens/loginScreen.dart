import 'package:flutter/material.dart';
import 'package:pdex_app/Helper/const.dart';
import 'package:pdex_app/Helper/ui_helper.dart';
import 'package:pdex_app/Screens/payment_details.dart';
import 'package:pdex_app/Screens/user_registration.dart';
import 'package:pdex_app/Screens/verify_phone.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool _obscureText = true;

//   void _submit() {
//     FocusScope.of(_ctx).requestFocus(new FocusNode());
//     final form = formKey.currentState;
//
//     if (form.validate()) {
//       UIHelper.requestLoader(_ctx);
//       form.save();
//       _presenter.getAccessToken(_username, _password,);
// //      _presenter.doLogin(_username, _password);
//     }
//   }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            new Container(
              padding: const EdgeInsets.only(top: 60.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                      width: 200,
                      height: 150,
                      /*decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(50.0)),*/
                      child: Image.asset('assets/images/logo.jpg')),
                  SizedBox(
                    height: 10.0,
                  ),
                  Text(
                    'Amain Express',
                    style: TextStyle(
                      fontSize: 40.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 25.0,
            ),
            new Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'খুব সহজেই রেজিস্ট্রেশন করুন! \n   ঘরে বসেই, মোবাইল দিয়ে ',
                    style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 25.0,
            ),
            Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'অনুগ্রহ করে আপনার ফোন নম্বর দিন',
                    style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(25, 2, 25, 0),
                    child: TextFormField(
                      keyboardType: TextInputType.phone,
                      autofocus: false,
                      // onSaved: (val) => _username = val,
                      validator: (val) {
                        return val.isEmpty ? "Invalid Phone Number" : null;
                      },
                      decoration: InputDecoration(
                        border: new OutlineInputBorder(
                          borderRadius: const BorderRadius.all(
                            const Radius.circular(3.0),
                          ),
                          borderSide: new BorderSide(
                            color: Colors.black,
                            width: 0.1,
                          ),
                        ),
                        errorStyle: TextStyle(color: Colors.red),
                        fillColor: Colors.white10,
                        filled: true,
                        prefixIcon: Icon(
                          Icons.phone,
                          color: Colors.black,
                        ),
                        hintText: '01XXXXXXXXX',
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                      ),
                    ),
                  ),

                ],
              ),
            ),
            SizedBox(height: 10.0,),
            Container(
              padding: EdgeInsets.fromLTRB(25, 2, 25, 0),
              child: new TextFormField(
                  autofocus: false,
                  obscureText: _obscureText,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    hintText: 'Password',
                    contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                    // border:
                    // OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),

                    border: new OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(3.0),
                      ),
                      borderSide: new BorderSide(
                        color: Colors.black,
                        width: 0.1,
                      ),
                    ),
                    suffixIcon: GestureDetector(
                      onTap: () {
                        setState(() {
                          _obscureText = !_obscureText;
                        });
                      },
                      child: Icon(
                        _obscureText ? Icons.visibility : Icons.visibility_off,
                        semanticLabel:
                        _obscureText ? 'show password' : 'hide password',
                      ),
                    ),
                  ),
                  // validator: FormValidator().validatePassword,
                  // onSaved: (String value) {
                  //   _loginData.password = value;
                  // }


                  ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Container(
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    new Align(
                        alignment: Alignment.center,
                        child: new ButtonTheme(
                            minWidth: 300.0,
                            height: 50.0,
                            child: new OutlineButton(
                              onPressed: () {

                                print('---=dddd---'+Const.HOST_NAME);

                                UIHelper.isInterNetConnectionAvailable()
                                    .then((bool isAvailable) {
                                  if (isAvailable) {
                                    print('---=dddd---'+Const.HOST_NAME);
                                    //_submit();
                                  }
                                });


                                Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => UserRegistration()));


                              },
                              child: Text("Login",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                      fontSize: 20.0,
                                      color: Colors.black,
                                      fontFamily: 'OpenSans')),
                              borderSide: BorderSide(color: Colors.black),
                              shape: StadiumBorder(),
                            ))),
                  ],
                ),
              ),
            ),

            SizedBox(height: 5.0,),
            new FlatButton(
              onPressed: () {
                Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => UserRegistration()));
              },
              child: Text('Not a member? Sign up now',
                  style: TextStyle(
                      fontSize: 15.0,
                      color: Colors.black54),

              ),
            ),

          ],
        ),
      ),
    );
  }
}
