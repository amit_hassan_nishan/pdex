import 'package:flutter/material.dart';
import 'package:pdex_app/Screens/registration_for_delivery.dart';
import 'package:pdex_app/Screens/settings.dart';

class DeliveryRules extends StatefulWidget {
  @override
  _DeliveryRulesState createState() => _DeliveryRulesState();
}

class _DeliveryRulesState extends State<DeliveryRules> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(

        leading: GestureDetector(
          onTap: () {
            Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => Settings()));
          },
          child: Icon(
            Icons.arrow_back,
            size: 30,
            color: Colors.black,
          ),
        ),
        title: Text('ডেলিভারি রেজিস্ট্রেশন শর্তাবলী'),
      ),
      body: Column(
        children: [
          Container(
            child: Text(
                'ডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলীডেলিভারি রেজিস্ট্রেশন শর্তাবলী'
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: RaisedButton(
              onPressed: () {
                Navigator.pushReplacement(context, MaterialPageRoute(
                    builder: (BuildContext context) => RegistrationForDelivery()));
              },
              child: const Text('I AGREE', style: TextStyle(fontSize: 20)),
              color: Colors.blue,
              textColor: Colors.white,
              elevation: 5,
            ),
          ),


        ],
      ),
    );
  }
}




/*class DeliveryRules extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: new Center(child: Text(
          'দোকানের তথ্য',
          style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),),

      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: Center(
                child: Container(
                    width: 200,
                    height: 60,
                    *//*decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(50.0)),*//*
                    child: Image.asset('assets/images/holding-phone.png')),
              ),
            ),
            Padding(
              //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextField(
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'আপনার নাম',
                    hintText: 'আপনার নাম'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 15.0, right: 15.0, top: 15, bottom: 0),
              //padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextField(

                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'দোকানের নাম',
                    hintText: 'দোকানের নাম'),
              ),
            ),

            Padding(
              padding: const EdgeInsets.only(
                  left: 15.0, right: 15.0, top: 15, bottom: 0),
              //padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextField(

                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'দোকানের ইমেইল',
                    hintText: 'দোকানের ইমেইল'),
              ),
            ),

            Padding(
              padding: const EdgeInsets.only(
                  left: 15.0, right: 15.0, top: 15, bottom: 0),
              //padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextField(

                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'দোকানের ঠিকানা',
                    hintText: 'দোকানের ঠিকানা'),
              ),
            ),

            Padding(
              padding: const EdgeInsets.only(
                  left: 15.0, right: 15.0, top: 15, bottom: 0),
              //padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextField(

                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'দোকানের নাম',
                    hintText: 'দোকানের নাম'),
              ),
            ),

            Padding(
              padding: const EdgeInsets.only(
                  left: 15.0, right: 15.0, top: 15, bottom: 0),
              //padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextField(

                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'একাউন্ট খুলার কারণ ?',
                    hintText: 'Enter secure password'),
              ),
            ),
            *//*Container(
        height: MediaQuery.of(context).size.height * 0.13,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(
            Radius.circular(25),
          ),
        ),
        child: Padding(
          padding: EdgeInsets.all(10),
          child: Row(
            children: <Widget>[

              Expanded(
                child: GestureDetector(
                  onTap: () {
                    print("NEXT");
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      color: Color(0xFFFFDC3D),
                      borderRadius: BorderRadius.all(
                        Radius.circular(15),
                      ),
                    ),
                    child: Center(
                      child: Text(
                        "NEXT",
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),

                    ),
                  ),
                ),
              ),

            ],
          ),
        ),
      ),*//*

            SizedBox(height: 10.0,),
            Container(
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    new Align(
                        alignment: Alignment.center,
                        child: new ButtonTheme(
                            minWidth: 300.0,
                            height: 50.0,
                            child: new OutlineButton(
                              onPressed: () {
                                Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => Settings()));
                              },
                              child: Text("NEXT",
                                  style:
                                  TextStyle(color: Colors.green, fontFamily: 'OpenSans')),
                              borderSide: BorderSide(color: Colors.black),
                              shape: StadiumBorder(
                              ),
                            ))),
                  ],
                ),
              ),

            ),

          ],
        ),
      ),

    );
  }
}*/


