import 'package:flutter/material.dart';
import 'package:pdex_app/Screens/opening_reason.dart';
import 'package:pdex_app/Screens/password_set.dart';


class PickUpInformation extends StatefulWidget {
  @override
  _PickUpInformationState createState() => _PickUpInformationState();
}

class _PickUpInformationState extends State<PickUpInformation> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: GestureDetector(
          onTap: () {
            Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => OpeningReason()));
          },
          child: Icon(
            Icons.arrow_back,
            size: 30,
            color: Colors.white,
          ),
        ),

        title: new Center(child: Text(
          'পিকাপের ডিটেলস',
          style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),),),

      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: Center(
                child: Container(
                    width: 200,
                    height: 100,
                    /*decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(50.0)),*/
                    child: Image.asset('assets/images/holding-phone.png')),
              ),
            ),
            Padding(
              //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextField(
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'পিকআপ ঠিকানা ',
                    hintText: 'পিকআপ ঠিকানা '),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 15.0, right: 15.0, top: 15, bottom: 0),
              //padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextField(
                keyboardType: TextInputType.text,
                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'পিকআপ এলাকা ',
                    hintText: 'পিকআপ এলাকা '),
              ),
            ),

            Padding(
              padding: const EdgeInsets.only(
                  left: 15.0, right: 15.0, top: 15, bottom: 0),
              //padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextField(
                keyboardType: TextInputType.number,
                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'পিকআপ ফোন নম্বর ',
                    hintText: 'পিকআপ ফোন নম্বর '),
              ),
            ),

            Padding(
              padding: const EdgeInsets.only(
                  left: 15.0, right: 15.0, top: 15, bottom: 0),
              //padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextField(
                keyboardType: TextInputType.text,
                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'প্রোডাক্ট এর ধরণ লিখুন ',
                    hintText: 'প্রোডাক্ট এর ধরণ লিখুন '),
              ),
            ),


            SizedBox(height: 15.0,),

            Container(
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    new Align(
                        alignment: Alignment.center,
                        child: new ButtonTheme(
                            minWidth: 300.0,
                            height: 50.0,
                            child: new OutlineButton(
                              onPressed: () {
                                Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => PasswordSet()));
                              },
                              child: Text("NEXT",
                                  style:
                                  TextStyle(color: Colors.green, fontFamily: 'OpenSans')),
                              borderSide: BorderSide(color: Colors.black),
                              shape: StadiumBorder(
                              ),
                            ))),
                  ],
                ),
              ),

            ),

          ],
        ),
      ),

    );
  }
}
